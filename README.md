# Notebooks

[Documentation](https://github.com/binder-examples/r/)

## View notebook originating from same domain

- [GitLab](https://about.gitlab.com/) | [![GitLab](https://img.shields.io/badge/view-GitLab-ff69b4.svg)](https://gitlab.com/fkohrt/Notebooks/blob/master/Histogramm_mit_R.ipynb)
- [Anaconda Cloud](https://anaconda.org/) | [![Anaconda-Server Badge](https://anaconda.org/fkohrt/histogramm_mit_r/badges/version.svg)](https://anaconda.org/fkohrt/histogramm_mit_r)

## View notebook originating from arbitrary location
- [nbviewer](https://nbviewer.jupyter.org/) | [![nbviewer](https://cdn.rawgit.com/jupyter/design/master/logos/Badges/nbviewer_badge.svg)](https://nbviewer.jupyter.org/urls/gitlab.com/fkohrt/Notebooks/raw/master/Histogramm_mit_R.ipynb): `https://nbviewer.jupyter.org/urls/gitlab.com/fkohrt/Notebooks/raw/master/Histogramm_mit_R.ipynb`
- [ZEPL](https://www.zepl.com/explore) | [![ZEPL](https://img.shields.io/badge/launch-ZEPL-ff69b4.svg)](https://www.zepl.com/viewer/urls/gitlab.com/fkohrt/Notebooks/raw/master/Histogramm_mit_R.ipynb): `https://www.zepl.com/viewer/urls/gitlab.com/fkohrt/Notebooks/raw/master/Histogramm_mit_R.ipynb`

## Run notebook originating from GitLab
- [Binder](https://mybinder.org/) | [![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/fkohrt%2FNotebooks/master?filepath=Histogramm_mit_R.ipynb): `https://mybinder.org/v2/gl/fkohrt%2FNotebooks/master?filepath=Histogramm_mit_R.ipynb`

## Run notebook originating from GitHub
- [Google Colab](https://research.google.com/colaboratory/) | [![Colab](https://img.shields.io/badge/launch-Colab-ff69b4.svg)](https://colab.research.google.com/github/fkohrt/Notebooks/blob/master/Histogramm_mit_R.ipynb): `https://colab.research.google.com/github/fkohrt/Notebooks/blob/master/Histogramm_mit_R.ipynb`